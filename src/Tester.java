import base.Constant;
import base.Expression;
import base.Fraction;
import base.Integer;
import matrices.Matrix;
import ui.MainScreen;

public class Tester {

	public static void main(String[] args) {

		/*Matrix matrixA = new Matrix(3, 3);
		matrixA.set(1, 1, new Fraction(1));
		matrixA.set(2, 1, new Fraction(0));
		matrixA.set(3, 1, new Fraction(0));
		matrixA.set(1, 2, new Fraction(0));
		matrixA.set(2, 2, new Fraction(1));
		matrixA.set(3, 2, new Fraction(0));
		matrixA.set(1, 3, new Fraction(0));
		matrixA.set(2, 3, new Fraction(0));
		matrixA.set(3, 3, new Fraction(1));

		Matrix matrixB = new Matrix(3, 4);
		matrixB.set(1, 1, new Fraction(6));
		matrixB.set(1, 2, new Fraction(2));
		matrixB.set(1, 3, new Fraction(2));
		matrixB.set(1, 4, new Fraction(-3));
		matrixB.set(2, 1, new Fraction(2));
		matrixB.set(2, 2, new Fraction(9));
		matrixB.set(2, 3, new Fraction(5));
		matrixB.set(2, 4, new Fraction(-2));
		matrixB.set(3, 1, new Fraction(2));
		matrixB.set(3, 2, new Fraction(5));
		matrixB.set(3, 3, new Fraction(4));
		matrixB.set(3, 4, new Fraction(-2));
		*/

		Expression expression = (new Integer(5).multiply(new Integer(3))).add(new Constant.PI().multiply(new Integer(8))).difference(new Integer(3));
		System.out.println(expression.toValue());
		System.out.println(expression.toString());

//		javax.swing.SwingUtilities.invokeLater(() -> new MainScreen(matrixA, matrixB));
	}

}

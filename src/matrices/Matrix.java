package matrices;

import base.Expression;
import base.Integer;
import vectors.Vector;

public class Matrix {

	private final int m;
	private final int n;

	private Vector[] data;

	public Matrix(int m, int n) {
		this.m = m;
		this.n = n;
		data = new Vector[m];
	}

	public Matrix(Matrix matrix) {
		this(matrix.getM(), matrix.getN());
		for (int j = 1; j <= m; j++) {
			set(j, matrix.get(j));
		}
	}

	public Matrix(Vector vector) {
		this(1, vector.getN());
		set(1, vector);
	}


	public void set(int j, int i, Expression value) {
		if (j < 1 || j > m || i < 1 || i > n)
			return;

		if (data[j - 1] == null)
			data[j - 1] = new Vector(n);

		data[j - 1].set(i, value);
	}

	public void set(int j, Vector vector) {
		if (j < 1 || j > m)
			return;

		data[j - 1] = vector;
	}

	public Expression get(int j, int i) throws MatrixIndexOutOfBounds {
		if (j < 1 || j > m)
			throw new MatrixIndexOutOfBounds();

		if (data[j - 1] == null)
			return new Integer(0);

		return data[j - 1].get(i);
	}

	public Vector get(int j) {
		if (j < 1 || j > m)
			return null;

		if (data[j - 1] == null)
			return new Vector(n);

		return data[j - 1];
	}

	public int getM() {
		return m;
	}

	public int getN() {
		return n;
	}

	public Matrix transposition() {
		Matrix transposition = new Matrix(getN(), getM());
		for (int j = 1; j <= m; j++) {
			for (int i = 1; i <= n; i++) {
				transposition.set(i, j, get(j, i));
			}
		}

		return transposition;
	}

	public void add(Matrix matrix) {
		for (int j = 1; j <= m; j++) {
			for (int i = 1; i <= n; i++) {
				get(j, i).add(matrix.get(j, i));
			}
		}
	}

	public Matrix sum(Matrix matrix) {
		Matrix newMatrix = new Matrix(this);
		newMatrix.add(matrix);
		return newMatrix;
	}

	public void multiply(Expression fraction) {
		for (int j = 1; j <= m; j++) {
			for (int i = 1; i <= n; i++) {
				get(j, i).multiply(fraction);
			}
		}
	}

	public Matrix product(Expression fraction) {
		Matrix newMatrix = new Matrix(this);
		newMatrix.multiply(fraction);
		return newMatrix;
	}

	public Matrix product(Matrix matrix) throws MatrixSizeException {
		if (matrix.getM() != getN())
			throw new MatrixSizeException();

		Matrix result = new Matrix(getM(), matrix.getN());

		for (int j = 1; j <= getM(); j++) {
			for (int i = 1; i <= matrix.getN(); i++) {
				Expression entry = new Integer(0);
				for (int k = 0; k < getN(); k++) {
					entry.add(get(j, k + 1).product(matrix.get(k + 1, i)));
				}
				result.set(j, i, entry);
			}
		}
		return result;
	}

	@Override
	public String toString() {
		StringBuilder sb = new StringBuilder();
		for (int j = 0; j < getM(); j++) {
			sb.append(j == 0 ? "┌ " : (j == getM() - 1 ? "└ " : "│ "));
			sb.append(get(j + 1));
			sb.append(j == 0 ? " ┐" : (j == getM() - 1 ? " ┘" : " │"));
			sb.append('\n');
		}
		return sb.toString();
	}
}

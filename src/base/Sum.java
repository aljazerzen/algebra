package base;

public class Sum extends Expression {

	private Expression a;
	private Expression b;

	public Sum(Expression a, Expression b) {
		this.a = a;
		this.b = b;
	}

	@SuppressWarnings("CloneDoesntCallSuperClone")
	@Override
	public Expression clone() {
		return new Sum(a.clone(), b.clone());
	}

	@Override
	public double toValue() {
		return a.toValue() + b.toValue();
	}

	@Override
	public String toString() {
		return "( " + a + "+ " + b + ") ";
	}
}

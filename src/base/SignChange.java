package base;

public class SignChange extends Expression {

	private Expression a;

	public SignChange(Expression a) {
		this.a = a;
	}

	@SuppressWarnings("CloneDoesntCallSuperClone")
	@Override
	public Expression clone() {
		return new SignChange(a.clone());
	}

	@Override
	public Expression changeSign() {
		return a;
	}

	@Override
	public double toValue() {
		return 0 - a.toValue();
	}

	@Override
	public String toString() {
		return "- " + a;
	}
}

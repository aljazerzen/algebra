package base;

public class Product extends Expression {

	private Expression a;
	private Expression b;

	public Product(Expression a, Expression b) {
		this.a = a;
		this.b = b;
	}

	@SuppressWarnings("CloneDoesntCallSuperClone")
	@Override
	public Expression clone() {
		return new Product(a.clone(), b.clone());
	}

	@Override
	public double toValue() {
		return a.toValue() * b.toValue();
	}

	@Override
	public String toString() {
		return "( " + a + "* " + b + ") ";
	}
}

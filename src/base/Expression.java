package base;

public abstract class Expression {

	Expression() {
	}

	@Override
	public abstract Expression clone();

	/*
	 * Returns an object that should be used instead of this one, which should not be used anymore.
	 * Argument objects are never changed.
	 */
	public Expression add(Expression expression) {
		// TODO new expression object should be created
		return new Sum(this, expression);
	}

	/*
	 * Returns an object that should be used instead of this one, which should not be used anymore.
	 * Argument objects are never changed.
	 */
	public Expression multiply(Expression expression) {
		// TODO new expression object should be created
		return new Product(this, expression);
	}

	/*
	 * Returns an object that should be used instead of this one, which should not be used anymore.
	 */
	public Expression changeSign() {
		return new SignChange(this);
	}

	/*
	 * Returns an object that should be used instead of this one, which should not be used anymore.
	 */
	public Expression inverse() {
		return new Inverse(this);
	}

	public Expression product(Expression expression) {
		return clone().multiply(expression);
	}

	public Expression difference(Expression expression) {
		return expression.clone().changeSign().add(this);
	}

	public Expression sum(Expression expression) {
		return clone().add(expression);
	}

	public abstract double toValue();

	@Override
	public abstract String toString();
}

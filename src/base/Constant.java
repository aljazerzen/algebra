package base;

public abstract class Constant extends Expression {

	String name;
	double value;

	@SuppressWarnings("CloneDoesntCallSuperClone")
	@Override
	public Expression clone() {
		return this;
	}

	@Override
	public double toValue() {
		return value;
	}

	@Override
	public String toString() {
		return name + " ";
	}

	public static class PI extends Constant {
		{
			name = "PI";
			value = Math.PI;
		}
	}

	public static class E extends Constant {
		{
			name = "E";
			value = Math.E;
		}
	}
}

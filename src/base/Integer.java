package base;

public class Integer extends Expression {

	long integer;

	public Integer(long integer) {
		this.integer = integer;
	}

	@SuppressWarnings("CloneDoesntCallSuperClone")
	@Override
	public Expression clone() {
		return new Integer(integer);
	}

	@Override
	public Expression add(Expression expression) {
		if (expression instanceof Integer) {
			integer += ((Integer) expression).integer;
			return this;
		}

		return super.add(expression);
	}

	@Override
	public Expression multiply(Expression expression) {
		if (expression instanceof Integer) {
			integer *= ((Integer) expression).integer;
			return this;
		}

		return super.multiply(expression);
	}

	@Override
	public Expression changeSign() {
		integer = 0 - integer;
		return this;
	}

	@Override
	public double toValue() {
		return integer;
	}

	@Override
	public String toString() {
		return Long.toString(integer) + " ";
	}
}

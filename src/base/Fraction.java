package base;

public class Fraction extends Expression {

	public static Fraction parseFraction(String s) throws NumberFormatException {
		int slash = s.indexOf('/');
		if (slash == -1) {
			return new Fraction(new Integer(java.lang.Long.parseLong(s)), new Integer(1));
		} else {
			return new Fraction(new Integer(java.lang.Long.parseLong(s.substring(0, slash))), new Integer(java.lang.Long.parseLong(s.substring(slash + 1))));
		}
	}

	private Expression numerator, denominator;

	public Fraction(Expression numerator, Expression denominator) {
		this.numerator = numerator;
		this.denominator = denominator;
	}

	public Fraction(Fraction fraction) {
		this(fraction.numerator, fraction.denominator);
	}

	@SuppressWarnings("CloneDoesntCallSuperClone")
	@Override
	public Expression clone() {
		return new Fraction(numerator.clone(), denominator.clone());
	}

	@Override
	public Expression add(Expression expression) {
		if (expression instanceof Fraction) {

			Fraction fraction = (Fraction) expression.clone();

			if (denominator instanceof Integer && fraction.denominator instanceof Integer) {
				long den = ((Integer) denominator).integer;
				long fra = ((Integer) fraction.denominator).integer;
				long gcd = GCD(den, fra);
				numerator = numerator.multiply(new Integer(fra * gcd)).add(fraction.numerator.multiply(new Integer(den * gcd)));
				return this;
			} else {
				numerator = numerator.multiply(fraction.denominator);
				fraction.numerator = fraction.numerator.multiply(denominator);
				numerator = numerator.add(fraction.numerator);
				denominator = denominator.multiply(fraction.denominator);
			}
		}
		if (expression instanceof Integer) {
			expression = expression.clone();
			numerator = numerator.add(expression.multiply(denominator));
			return this;
		}

		return super.add(expression);
	}

	@Override
	public Expression multiply(Expression expression) {
		if (expression instanceof Fraction) {
			numerator = numerator.multiply(((Fraction) expression).numerator);
			denominator = denominator.multiply(((Fraction) expression).denominator);
			return this;
		}
		if (expression instanceof Integer) {
			numerator = numerator.multiply(expression);
			return this;
		}
		return super.multiply(expression);
	}

	public Expression inverse() {
		Expression t = numerator;
		numerator = denominator;
		denominator = t;
		return this;
	}

	@Override
	public double toValue() {
		return numerator.toValue() / denominator.toValue();
	}

	private void shorten() {
		if (numerator instanceof Integer && denominator instanceof Integer) {
			long numerator = ((Integer) this.numerator).integer;
			long denominator = ((Integer) this.denominator).integer;

			if (denominator < 0) {
				numerator = 0 - numerator;
				denominator = 0 - denominator;
			}

			long gcd = GCD(numerator, denominator);
			this.numerator = new Integer(numerator / gcd);
			this.denominator = new Integer(denominator / gcd);
		}
	}

	private static long GCD(long a, long b) {
		if (a < 0)
			a = 0 - a;
		if (b < 0)
			b = 0 - b;
		while (b > 0) {
			long t = a;
			a = b;
			b = t % b;
		}
		return a;
	}

	/*@Override
	public int compareTo(Fraction fraction) {
		fraction = (Fraction) fraction.clone();
		fraction.numerator = fraction.numerator.multiply(denominator);
		fraction.denominator = fraction.denominator.multiply(numerator);
		if ( == )
			return 0;

		if (fraction.numerator * denominator < numerator * fraction.denominator)
			return -1;

		return 1;
	}*/

	@Override
	public String toString() {
		shorten();
		return numerator.toString() + "/" + denominator.toString();
	}
}

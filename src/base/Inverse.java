package base;

public class Inverse extends Expression {

	private Expression a;

	public Inverse(Expression a) {
		this.a = a;
	}

	@Override
	public Expression clone() {
		return new Inverse(a);
	}

	@Override
	public double toValue() {
		return 1 / a.toValue();
	}

	@Override
	public String toString() {
		return "( 1 / " + a + ") ";
	}
}

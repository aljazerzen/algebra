package vectors;

import base.Expression;
import base.Integer;

public class Vector {

	private final int n;

	private Expression[] data;

	public Vector(int n) {
		this.n = n;
		data = new Expression[n];
	}

	public void set(int i, Expression value) {
		if (i > 0 && i <= n)
			data[i - 1] = value;
	}

	public Expression get(int i) throws VectorOutOfBoundsException {
		if (i < 1 || i > n)
			throw new VectorOutOfBoundsException();

		if (data[i - 1] == null) {
			return new Integer(0);
		}

		return data[i - 1];
	}

	public int getN() {
		return n;
	}

	public void add(Vector vector) throws VectorSizeException {
		if (vector.n != n) {
			throw new VectorSizeException();
		}

		for (int i = 0; i < data.length; i++) {
			data[i].add(vector.get(i));
		}
	}

	public void multiply(Expression expression) {
		for (Expression aData : data) {
			aData.multiply(expression);
		}
	}

	public Expression dotProduct(Vector vector) {
		Expression r = new Integer(0);
		for (int i = 0; i < data.length; i++) {
			r.add(data[i].product(vector.get(i)));
		}
		return r;
	}

	public Vector crossProduct(Vector vector) throws VectorSizeException {
		if (vector.getN() != 3 || n != 3) {
			throw new VectorSizeException();
		}

		Vector r = new Vector(3);
		r.set(1, get(2).product(vector.get(3)).difference(get(3).product(vector.get(2))));
		r.set(2, get(3).product(vector.get(1)).difference(get(1).product(vector.get(3))));
		r.set(3, get(1).product(vector.get(2)).difference(get(2).product(vector.get(1))));
		return r;
	}

	public double getNorm() {
		double norm = 0;
		for (Expression expression : data) {
			norm += expression.toValue() * expression.toValue();
		}
		return Math.sqrt(norm);
	}

	@Override
	public String toString() {
		StringBuilder sb = new StringBuilder();
		for (int i = 0; i < n; i++) {
			sb.append(get(i + 1).toString());
			if (i + 1 < n) {
				sb.append(" ");
			}
		}
		return sb.toString();
	}
}

package ui;

import java.awt.Color;
import java.awt.GridLayout;

import javax.swing.BorderFactory;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JTextField;
import javax.swing.JTextPane;
import javax.swing.border.EmptyBorder;
import javax.swing.event.DocumentListener;
import javax.swing.plaf.BorderUIResource;

public class MatrixInput {
	private JPanel rootMatrixPanel;

	public MatrixInput(int m, int n, DocumentListener listener) {

		rootMatrixPanel = new JPanel();

		GridLayout gridLayout = new GridLayout(m, n, 15, 15);
		rootMatrixPanel.setLayout(gridLayout);

		for (int j = 0; j < m; j++) {
			for (int i = 0; i < n; i++) {
				JTextField jTextField = new JTextField(3);
				jTextField.getDocument().addDocumentListener(listener);
				rootMatrixPanel.add(jTextField);
			}
		}
	}

	public JPanel getRootPanel() {
		return rootMatrixPanel;
	}
}

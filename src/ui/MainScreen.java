package ui;

import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.JTextField;
import javax.swing.WindowConstants;
import javax.swing.event.DocumentEvent;
import javax.swing.event.DocumentListener;

import base.Fraction;
import base.Integer;
import matrices.Matrix;
import matrices.MatrixSizeException;

public class MainScreen extends JFrame {

	private JPanel rootPanel;
	private JTextField A_m_input;
	private JTextField A_n_input;
	private JTextField B_m_input;
	private JTextField B_n_input;
	private JPanel A_input_matrix;
	private JPanel B_input_matrix;
	private JPanel AB_matrix;

	private DocumentListener inputListener = new DocumentListener() {
		public void changedUpdate(DocumentEvent e) {
			inputChanged();
		}

		public void removeUpdate(DocumentEvent e) {
			inputChanged();
		}

		public void insertUpdate(DocumentEvent e) {
			inputChanged();
		}
	};

	public MainScreen(Matrix matrixA, Matrix matrixB) {
		this();
		loadA(matrixA);
		loadB(matrixB);
	}

	public MainScreen() {
		super("Hello world!");
		setContentPane(rootPanel);

		setDefaultCloseOperation(WindowConstants.EXIT_ON_CLOSE);
		setVisible(true);

		DocumentListener AinputSizeInputListener = new DocumentListener() {
			public void changedUpdate(DocumentEvent e) {
				AinputSizeInputChanged();
			}

			public void removeUpdate(DocumentEvent e) {
				AinputSizeInputChanged();
			}

			public void insertUpdate(DocumentEvent e) {
				AinputSizeInputChanged();
			}
		};
		DocumentListener BinputSizeInputListener = new DocumentListener() {
			public void changedUpdate(DocumentEvent e) {
				BinputSizeInputChanged();
			}

			public void removeUpdate(DocumentEvent e) {
				BinputSizeInputChanged();
			}

			public void insertUpdate(DocumentEvent e) {
				BinputSizeInputChanged();
			}
		};
		A_m_input.getDocument().addDocumentListener(AinputSizeInputListener);
		A_n_input.getDocument().addDocumentListener(AinputSizeInputListener);
		B_m_input.getDocument().addDocumentListener(BinputSizeInputListener);
		B_n_input.getDocument().addDocumentListener(BinputSizeInputListener);

		AinputSizeInputChanged();
		BinputSizeInputChanged();
		inputChanged();
	}

	private static int getInt(JTextField textField) {
		try {
			return java.lang.Integer.parseInt(textField.getText());
		} catch (NumberFormatException ignored) {
			return 0;
		}
	}

	private static Fraction getFraction(JTextField textField) {
		try {
			return Fraction.parseFraction(textField.getText());
		} catch (NumberFormatException e) {
			return new Fraction(new Integer(0), new Integer(1));
		}
	}

	public void loadA(Matrix matrix) {
		load(matrix, A_m_input, A_n_input, A_input_matrix);
	}

	public void loadB(Matrix matrix) {
		load(matrix, B_m_input, B_n_input, B_input_matrix);
	}

	private void load(Matrix matrix, JTextField m_input, JTextField n_input, JPanel input_matrix) {
		m_input.setText("" + matrix.getM());
		n_input.setText("" + matrix.getN());

		try {
			for (int j = 0; j < matrix.getM(); j++) {
				for (int i = 0; i < matrix.getN(); i++) {
					((JTextField) ((JPanel) input_matrix.getComponent(0))
							.getComponent(j * matrix.getN() + i))
							.setText("" + matrix.get(j + 1, i + 1));
				}
			}
		} catch (IndexOutOfBoundsException e) {
			return;
		}
	}

	private void AinputSizeInputChanged() {
		int Am = getInt(A_m_input), An = getInt(A_n_input);
		if (Am > 0 && An > 0) {
			A_input_matrix.removeAll();
			A_input_matrix.add(new MatrixInput(Am, An, inputListener).getRootPanel());
		}
		pack();
	}

	private void BinputSizeInputChanged() {
		int Bm = getInt(B_m_input), Bn = getInt(B_n_input);
		if (Bm > 0 && Bn > 0) {
			B_input_matrix.removeAll();
			B_input_matrix.add(new MatrixInput(Bm, Bn, inputListener).getRootPanel());
		}
		pack();
	}

	private void inputChanged() {
		int Am = getInt(A_m_input), An = getInt(A_n_input);
		int Bm = getInt(B_m_input), Bn = getInt(B_n_input);

		Matrix matrixA = new Matrix(Am, An);
		try {
			for (int j = 0; j < Am; j++) {
				for (int i = 0; i < An; i++) {
					Fraction f = getFraction((JTextField) ((JPanel) A_input_matrix.getComponent(0)).getComponent(j * An + i));
					matrixA.set(j + 1, i + 1, f);
				}
			}
		} catch (IndexOutOfBoundsException e) {
			return;
		}

		Matrix matrixB = new Matrix(Bm, Bn);
		try {
			for (int j = 0; j < Bm; j++) {
				for (int i = 0; i < Bn; i++) {
					Fraction f = getFraction((JTextField) ((JPanel) B_input_matrix.getComponent(0)).getComponent(j * Bn + i));
					matrixB.set(j + 1, i + 1, f);
				}
			}
		} catch (IndexOutOfBoundsException e) {
			return;
		}

		Matrix product;
		try {
			product = matrixA.product(matrixB);
		} catch (MatrixSizeException e) {
			return;
		}

		AB_matrix.removeAll();
		AB_matrix.add(new ui.Matrix(product).getRootPanel());
		pack();
	}
}

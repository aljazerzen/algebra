package ui;

import java.awt.Color;
import java.awt.GridLayout;

import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.border.EmptyBorder;
import javax.swing.plaf.BorderUIResource;

public class Matrix {
	private JPanel rootMatrixPanel;

	public Matrix(matrices.Matrix matrix) {

		rootMatrixPanel = new JPanel();
		rootMatrixPanel.setBorder(new BorderUIResource.MatteBorderUIResource(0, 1, 0, 1, Color.BLACK));

		JPanel innerPanel = new JPanel();
		innerPanel.setBorder(new EmptyBorder(0, 0, 0, 0));
		GridLayout gridLayout = new GridLayout(matrix.getM(), matrix.getN(), 15, 15);
		innerPanel.setLayout(gridLayout);


		for (int j = 0; j < matrix.getM(); j++) {

			for (int i = 0; i < matrix.getN(); i++) {

				JLabel label = new JLabel(matrix.get(j + 1, i + 1).toString());
				innerPanel.add(label);

			}
		}
		rootMatrixPanel.add(innerPanel);
	}

	public JPanel getRootPanel() {
		return rootMatrixPanel;
	}
}
